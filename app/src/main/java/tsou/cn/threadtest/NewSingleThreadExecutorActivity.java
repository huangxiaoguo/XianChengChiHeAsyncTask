package tsou.cn.threadtest;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 创建一个单线程化的线程池，它只会用唯一的工作线程来执行任务，
 * 保证所有任务按照指定顺序(FIFO, LIFO, 优先级)执行
 * <p>
 * 这个线程池只有一个线程在工作，也就是相当于单线程串行执行>所有任务。
 * 如果这个唯一的线程因为异常结束，那么会有一个新的线程来替代它。
 * 此线程池>保证所有任务的执行顺序按照任务的提交顺序执行。
 */
public class NewSingleThreadExecutorActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * 开始
     */
    private Button mStart;
    private ExecutorService singleThreadExecutor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_btn);
        initData();
        initView();
    }

    private void initData() {
        singleThreadExecutor = Executors.newSingleThreadExecutor();
    }

    private void initView() {
        mStart = (Button) findViewById(R.id.start);
        mStart.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.start:
                for (int i = 0; i < 20; i++) {
                    final int m = i;
                    singleThreadExecutor.execute(new Runnable() {
                        @Override
                        public void run() {
                            SystemClock.sleep(2000);
                            if (singleThreadExecutor.isShutdown()) {
                                return;
                            }
                            Log.e("huangxiaoguo", "huangxiaoguo" + m);
                        }
                    });
                }
                break;
        }
    }

    /**
     * 停止线程工作
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //threadPool 不能为空 threadPool 没有崩溃 threadPool 没有停止
        if (singleThreadExecutor != null && !singleThreadExecutor.isShutdown()
                && !singleThreadExecutor.isTerminated()) {
            singleThreadExecutor.shutdownNow();
        }
    }
}
