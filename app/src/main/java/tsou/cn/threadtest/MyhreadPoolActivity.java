package tsou.cn.threadtest;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import tsou.cn.threadtest.utils.ThreadUtils;

/***
 * 自定义线程池
 */
public class MyhreadPoolActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * 开始
     */
    private Button mStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_btn);
        initView();
    }

    private void initView() {
        mStart = (Button) findViewById(R.id.start);
        mStart.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.start:
                for (int i = 0; i < 10; i++) {//不能超过最大队列缓存数，不然崩溃(我的ThreadPoolManager最大缓存队列为10)
                    final int m = i;
                    ThreadUtils.runOnLongBackThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("huangxiaoguo", "huangxiaoguo" + m);
                            SystemClock.sleep(2000);
                        }
                    });
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ThreadUtils.cancelLongBackThread();
    }
}
