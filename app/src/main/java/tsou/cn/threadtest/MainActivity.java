package tsou.cn.threadtest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

/**
 * Java通过Executors提供四种线程池，分别为：
 * <p>
 * newCachedThreadPool创建一个可缓存线程池，如果线程池长度超过处理需要，
 * 可灵活回收空闲线程，若无可回收，则新建线程。
 * <p>
 * newFixedThreadPool 创建一个定长线程池，可控制线程最大并发数，超出的线程会在队列中等待。
 * <p>
 * newScheduledThreadPool 创建一个定长线程池，支持定时及周期性任务执行。
 * <p>
 * newSingleThreadExecutor 创建一个单线程化的线程池，它只会用唯一的工作线程来执行任务，
 * 保证所有任务按照指定顺序(FIFO, LIFO, 优先级)执行。
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * 线程池..newFixedThreadPool
     */
    private TextView mNewFixedThreadPool;
    /**
     * 线程池结合AsyncTask
     */
    private TextView mAsyncTaskAndPool;
    /**
     * 线程池.newScheduledThreadPool
     */
    private TextView mNewScheduledThreadPool;
    /**
     * 线程池.newCachedThreadPool
     */
    private TextView mNewCachedThreadPool;
    /**
     * 线程池.newSingleThreadExecutor
     */
    private TextView mNewSingleThreadExecutor;
    /**
     * 自定义线程池myThreadPoolManager
     */
    private TextView mMyThreadPoolManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        mNewFixedThreadPool = (TextView) findViewById(R.id.newFixedThreadPool);
        mNewFixedThreadPool.setOnClickListener(this);
        mAsyncTaskAndPool = (TextView) findViewById(R.id.asyncTask_and_pool);
        mAsyncTaskAndPool.setOnClickListener(this);
        mNewScheduledThreadPool = (TextView) findViewById(R.id.newScheduledThreadPool);
        mNewScheduledThreadPool.setOnClickListener(this);
        mNewCachedThreadPool = (TextView) findViewById(R.id.newCachedThreadPool);
        mNewCachedThreadPool.setOnClickListener(this);
        mNewSingleThreadExecutor = (TextView) findViewById(R.id.newSingleThreadExecutor);
        mNewSingleThreadExecutor.setOnClickListener(this);

        mMyThreadPoolManager = (TextView) findViewById(R.id.myThreadPoolManager);
        mMyThreadPoolManager.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.asyncTask_and_pool:
                startActivity(new Intent(this, AsyncTaskActivity.class));
                break;
            case R.id.newFixedThreadPool:
                startActivity(new Intent(this, NewFixedThreadPoolActivity.class));
                break;
            case R.id.newScheduledThreadPool:
                startActivity(new Intent(this, NewScheduledThreadPoolActivity.class));
                break;
            case R.id.newCachedThreadPool:
                startActivity(new Intent(this, NewCachedThreadPoolActivity.class));
                break;
            case R.id.newSingleThreadExecutor:
                startActivity(new Intent(this, NewSingleThreadExecutorActivity.class));
                break;
            case R.id.myThreadPoolManager:
                startActivity(new Intent(this, MyhreadPoolActivity.class));
                break;
        }
    }
}
