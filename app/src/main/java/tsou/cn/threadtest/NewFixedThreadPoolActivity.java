package tsou.cn.threadtest;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/***
 * newFixedThreadPool 创建一个定长线程池，可控制线程最大并发数，超出的线程会在队列中等待
 *
 * 每次提交一个任务就创建一个线程，直到线程达到线程池的最大大小。
 * 线程池的大小一旦达到最大值就会保持不变，如果某个线程因为执行异常而结束，
 * 那么线程池会补充一个新线程。
 */
public class NewFixedThreadPoolActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * 开始
     */
    private Button mStart;
    private ExecutorService fixedThreadPool;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_btn);
        initData();
        initView();
    }

    private void initData() {
        fixedThreadPool = Executors.newFixedThreadPool(3);
    }

    private void initView() {
        mStart = (Button) findViewById(R.id.start);
        mStart.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.start:
                for (int i = 0; i < 20; i++) {
                    final int m = i;
                    fixedThreadPool.execute(new Runnable() {
                        @Override
                        public void run() {
                            /**
                             * 因为线程池大小为3，每个任务输出index后sleep 2秒，所以每两秒打印3个数字。
                             定长线程池的大小最好根据系统资源进行设置。
                             */
                            if (fixedThreadPool.isShutdown()) {
                                return;
                            }
                            Log.e("huangxiaoguo", "huangxiaoguo" + m);
                            SystemClock.sleep(2000);
                        }
                    });
                }
                break;
        }
    }

    /**
     * 停止线程工作
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //threadPool 不能为空 threadPool 没有崩溃 threadPool 没有停止
        if (fixedThreadPool != null && !fixedThreadPool.isShutdown()
                && !fixedThreadPool.isTerminated()) {
            fixedThreadPool.shutdownNow();
        }
    }
}
