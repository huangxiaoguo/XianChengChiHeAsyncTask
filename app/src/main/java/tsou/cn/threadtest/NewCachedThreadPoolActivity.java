package tsou.cn.threadtest;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 创建一个可缓存线程池，如果线程池长度超过处理需要，可灵活回收空闲线程，若无可回收，则新建线程。
 * <p>
 * 如果线程池的大小超过了处理任务所需要的线程，
 * 那么就会回收部分空闲（60秒不执行任务）的线程，当任务数增加时，
 * 此线程池又可以智能的添加新线程来处理任务。
 * 此线程池不会对线程池大小做限制，
 * 线程池大小完全依赖于操作系统（或者说JVM）能够创建的最大线程大小。
 */
public class NewCachedThreadPoolActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * 开始
     */
    private Button mStart;
    private ExecutorService cachedThreadPool;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_btn);
        initData();
        initView();
    }

    private void initData() {
        cachedThreadPool = Executors.newCachedThreadPool();
    }

    private void initView() {
        mStart = (Button) findViewById(R.id.start);
        mStart.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.start:
                for (int i = 0; i < 20; i++) {
                    final int m = i;
                    cachedThreadPool.execute(new Runnable() {
                        @Override
                        public void run() {
                            SystemClock.sleep(2000);
                            if (cachedThreadPool.isShutdown()) {
                                return;
                            }
                            Log.e("huangxiaoguo", "huangxiaoguo" + m);
                        }
                    });
                }
                break;
        }
    }

    /**
     * 停止线程工作
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //threadPool 不能为空 threadPool 没有崩溃 threadPool 没有停止
        if (cachedThreadPool != null && !cachedThreadPool.isShutdown()
                &&!cachedThreadPool.isTerminated()) {
            cachedThreadPool.shutdownNow();
        }
    }
}
